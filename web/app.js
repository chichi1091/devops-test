const http = require('http')
const fs = require('fs')

const server = http.createServer(function(req, res) {
  let url = req.url
  if (url === '/' || url === '') {
    url = '/index.html'
  }
  url = 'public' + url
  console.log('Receive request on : ' + process.env.ENVIRONMENT + '.  [URL] ' + url + '  [Cookie] ' + JSON.stringify(req.headers.cookie))
  if (fs.existsSync(url)) {
    fs.readFile(url, 'utf-8', (err, data) => {
      if (!err) {
        data = data.split('gray').join('greenyellow')
        data = data
          .split('%MESSAGE%')
          .join(
            'Hello, DevOps World!! your environment is ' +
              process.env.ENVIRONMENT
          )
        // console.log(data)
        res.writeHead(200, { 'Content-Type': 'text/html' })
        res.end(data)
      } else {
        res.writeHead(500, { 'Content-Type': 'text/html' })
        res.end('Internal Server Error')
      }
    })
  }
})

const port = process.env.PORT || 80
server.listen(port, function() {
  console.log(
    'To view your app, open this link in your browser: http://{EXTERNAL-IP}:' +
      port
  )
})
